const createInputParamCircle = document.querySelector('.create-interface');

const createCirclesCssName = 'create-circles';
const contentCirclesCssName = 'content-circles';
const itemCircleCssName = 'circle';
const countItem = 100;
let contentDiv;
let inputDiametrValue;

function createInterfaceContent() {
  const interface = createDiv('interface');
  createInputParamCircle.after(interface);

  inputDiametrValue = createInput('diametr-value');
  interface.append(inputDiametrValue);

  const createCircleBtn = createButton(createCirclesCssName);
  createCircleBtn.textContent = 'Нарисовать';
  interface.append(createCircleBtn);
  createCircleBtn.addEventListener('click', createCircleItems);

  contentDiv = createDiv(contentCirclesCssName);
  interface.after(contentDiv);
  contentDiv.addEventListener('click', deleteCircleItems);
}

function createCircleItems() {
  for (let i = 0; i < countItem; i++) {
    const itemCircle = createDiv(itemCircleCssName);
    itemCircle.style.backgroundColor = `hsl(${randomColor()} 80% 40%)`;
    console.log(inputDiametrValue);
    console.log(inputDiametrValue.value);
    itemCircle.style.height = `${inputDiametrValue.value || 40}px`;
    itemCircle.style.width = `${inputDiametrValue.value || 40}px`;

    contentDiv.append(itemCircle);
  }
}

function deleteCircleItems(e) {
  if (e.target.classList.contains(itemCircleCssName)) {
    e.target.remove();
  }
}

function randomColor() {
  return parseInt(Math.random() * 360);
}

function createDiv(className) {
  const div = document.createElement('div');
  div.classList.add(className);
  return div;
}

function createButton(className) {
  const button = document.createElement('button');
  button.classList.add(className);
  return button;
}

function createInput(className) {
  const input = document.createElement('input');
  input.classList.add(className);
  return input;
}

createInputParamCircle.addEventListener('click', createInterfaceContent);
// createInputParamCircle.click();
